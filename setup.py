from setuptools import setup, find_packages

setup(name='ispconfig_borgmatic_confbuilder',
      version='0.2',
      description='Lists the ispconfig databases or website directories (optionally with lxc rootfs) in yml to be added in borgmatic conf',
      url='http://gitlab.com/communia/ispconfig_borgmatic_confbuilder',
      author='Aleix Quintana Alsius',
      author_email='kinta@communia.org',
      license='GPL',
      packages=["ispconfig_borgmatic"], #['ispconfig_borg_borgmatic'],
      scripts=['bin/ispconfig_borgmatic_confbuilder'],
      zip_safe=False)
